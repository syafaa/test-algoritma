function test(str) {
  let data = [];

  for (let i = 0; i < str.length; i++) {
    if (str[i] == "(" || str[i] == "[" || str[i] == "{") {
      data.push(str[i]);
    } else {
      let obj = data.pop();
      if (
        !(
          (str[i] == ")" && obj == "(") ||
          (str[i] == "]" && obj == "[") ||
          (str[i] == "}" && obj == "{")
        )
      ) {
        return false;
      }
    }
  }
  return data.length == 0;
}

console.log(test("({[]})")); // true
console.log(test("([][]{})")); // true
console.log(test("({)(]){[}")); //false
console.log(test("[)()]")); //false
console.log(test("({})[]([])")); // true
console.log(test("{(})[]{)(}")); //false
